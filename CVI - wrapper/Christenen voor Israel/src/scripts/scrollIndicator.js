import gsap from "../../../config/node_modules/gsap";
import ScrollTrigger from "../../../config/node_modules/gsap/ScrollTrigger";

async function scrollIndicator() {

    var scrollIndicator = document.querySelector('.scroll-indicator');
    if (typeof(scrollIndicator) === 'undefined' || scrollIndicator === null || scrollIndicator.length === 0) return;
    
    gsap.registerPlugin(ScrollTrigger);

    var scrollIndicatorInner = scrollIndicator.querySelector(".scroll-indicator__inner");
    var scrolledContent = document.querySelector(".single-artikel");

    ScrollTrigger.create({
        trigger: scrolledContent,
        start: "top top",
        end: "bottom bottom",
        scrub: 0.25,
        invalidateOnRefresh: true,
        onUpdate: (self) => {
            var progress = parseFloat((self.progress).toFixed(2));

            gsap.to(scrollIndicatorInner, {
                scaleX: () => { return progress },
                ease: "Power2.ease",
            })
        },
    });
    

}
    

export {
    scrollIndicator
}


