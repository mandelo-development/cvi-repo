import gsap from "../../../config/node_modules/gsap";

async function showMore() {

    var filterLists = document.querySelectorAll('.filter--expand');
    if (typeof(filterLists) === 'undefined' || filterLists === null || filterLists.length === 0) return;

    filterLists.forEach(filterList => {
        // var itemsList = filterList.querySelector(".filter-checkbox__group");
        // var items = itemsList.querySelectorAll(".filter-checkbox__item")
        // var showMore = filterList.querySelector(".show-more");
        
        // let expanded = false;
        
        // showMore.addEventListener('click', () => {
        //   expanded = !expanded;
        
        //   const targetHeight = expanded ? itemsList.scrollHeight : 4 * getItemHeight(items[0]);
        
        //   gsap.to(itemsList, {
        //     height: targetHeight,
        //     duration: 0.5,
        //     ease: 'Power3.out',
        //   });
        
        //   showMore.textContent = expanded ? 'Toon minder' : 'Toon meer';
        // });

        const expandButton = filterList.querySelector('.filter__expand');
        const expandButtonText = expandButton.querySelector("span");
        const expandButtonSvg = expandButton.querySelector("svg");
        const itemGroup = filterList.querySelector('.filter__group');
        const items = Array.from(filterList.querySelectorAll(".filter__item"));
        const hiddenItems = Array.from(filterList.querySelectorAll('.filter__item--hidden'));;

        const totalShownItemsHeight = items.reduce(
            (totalHeight, item) => totalHeight + getItemHeight(item),
            0
        );

        let isExpanded = false;

        expandButton.addEventListener('click', () => {
            isExpanded = !isExpanded;

            if (isExpanded) {
                showHiddenItems();
            } else {
                hideHiddenItems();
            }

            

            // expandButtonText.textContent = isExpanded ? 'Toon minder' : 'Toon meer';

            // console.log(itemGroup.scrollHeight);
            updateButtonText();
        });

        function updateButtonText() {
            const dataAttribute = isExpanded ? 'data-shown' : 'data-hidden';
            const rotateSvg = isExpanded ? -180 : 0;
            expandButtonText.textContent = expandButtonText.getAttribute(dataAttribute);
            gsap.to(expandButtonSvg, {
                rotate: rotateSvg,
                duration: .4,
                ease: "Power3.out"
            })
        }

        function showHiddenItems() {
            gsap.set(hiddenItems, { opacity: 0, display: 'inline-flex' });

            const totalHiddenItemsHeight = hiddenItems.reduce(
                (totalHeight, item) => totalHeight + getItemHeight(item),
                0
            );

            gsap.to(hiddenItems, {
                opacity: 1,
                stagger: 0.4 / hiddenItems.length,
                duration: 0.4,
            });
            gsap.fromTo(itemGroup, {
                height: totalShownItemsHeight,
            }, {
                height: totalShownItemsHeight + totalHiddenItemsHeight,
                duration: 0.4,
                ease: "Power3.out"
            });
        }

        function hideHiddenItems() {
            gsap.to(hiddenItems, {
                opacity: 0,
                display: "none",
                duration: 0.4,
            });
            gsap.to(itemGroup, {
                height: totalShownItemsHeight,
                duration: 0.4,
                ease: "Power3.out"
            });
        }
                
        function getItemHeight(item) {
          return item.scrollHeight;
        }
        
    });
}
    

export {
    showMore
}


