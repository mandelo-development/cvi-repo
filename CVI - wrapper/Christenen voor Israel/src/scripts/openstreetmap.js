async function openstreetmap () {

  var $ = require("../../../config/node_modules/jquery");
  var L = require("../../../config/node_modules/leaflet");
  var markers;

  $(document).ready(function () {

    window.onload = function () {
      if ($("#maps-container")[0]){
       
        mapInit();
        
      }
    };

    function mapInit() {
      var map = L.map('maps-container', { scrollWheelZoom: false, }).setView([52.092876, 5.104480], 10);
      markers = new L.FeatureGroup();
      map.addLayer(markers);
      var counter = 0;

      L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager_nolabels/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(map);

      L.tileLayer('https://{s}.basemaps.cartocdn.com/rastertiles/voyager_only_labels/{z}/{x}/{y}.png').addTo(map);
      var locations = JSON.parse(document.getElementById("maps-container").dataset.locations);

      for (let x = 0; x < locations.length; x++) {
        setTimeout( function timer() {
          geocodeAddress(locations[x], map);
        }, x*1010);
      }
    }

    function geocodeAddress(location, map) {
      var zip_code = location[4].replace(/\s/g, '').slice(0, 6);
      var full_address = location[1] + ' ' + location[2] + ' ' + zip_code + ' ' + location[3];

      ajax_request(
          "https://nominatim.openstreetmap.org/search?format=json&limit=1&q=" + full_address,
          response => mapPopup(response, map, location)
      )
    }

    function mapPopup(xhr_response, map, location_info) {

      var result = JSON.parse(xhr_response)[0];
      var LeafIcon = L.Icon.extend({
        options: {
          iconAnchor:   [33, 30],
          shadowAnchor: [11, 15],
        }
      });

      var svgIcon = new LeafIcon({
        iconUrl: '/theme/assets/images/marker-icon.svg',
        shadowUrl: '/theme/assets/images/marker-shadow.png'
      });

      if (result) {
        var lat = result.lat;
        var lon = result.lon;

        L.marker([lat, lon], {icon: svgIcon}).addTo(markers);
        map.flyTo({lon: lon, lat: lat}, 15);
      }
    }

    function ajax_request(url, cb) {
      var xhttp = new XMLHttpRequest();
      xhttp.open("GET", url, true);
      xhttp.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
          return cb( this.responseText )
        }
      };
      xhttp.send()
    }
  });
} export {
  openstreetmap
}