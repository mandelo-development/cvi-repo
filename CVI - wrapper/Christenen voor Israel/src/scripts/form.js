async function form() {
  let forms = document.querySelectorAll(
    "#donate_form form, #sign_in_form form, #general_paymentform"
  );
  var focused = false;
  var hasExtraAttendees = false;

  for (var i = 0; i < forms.length; i++) {
    let $thisForm = forms[i];
    let formInputs = forms[i].querySelectorAll(
      ".form-field__input, .form-field__select, #donate_anonymous, #amount_10, #amount_25, #amount_50"
    );

    if (!focused) {
      formInputs.forEach((input) => {
        input.addEventListener("focus", () => {
          reCaptchaFocus();
        });
        input.addEventListener("change", () => {
          reCaptchaFocus();
        });
      });
    }

    var invoiceToggle = forms[i].querySelector(".option-authority-field");
    if (invoiceToggle !== null && invoiceToggle !== null) {
      var CountrySelect = $thisForm.querySelector(
        "select[name='default_fields[land]']"
      );
      CountrySelect.addEventListener("change", (e) => {
        if (CountrySelect.value == "NL") {
          document.querySelector(".form-field__bic").style.display = "none";
        } else {
          document.querySelector(".form-field__bic").style.display = "block";
        }
      });

      var invoiceToggleFields = invoiceToggle.querySelectorAll(".option");
      for (var f = 0; f < invoiceToggleFields.length; f++) {
        invoiceToggleFields[f].addEventListener("click", function (e) {
          setTimeout(() => {
            if (invoiceToggleFields[1].querySelector("input").checked == true) {
              document.querySelector(".expand-iban-field").style.display =
                "block";
            } else {
              document.querySelector(".expand-iban-field").style.display =
                "none";
            }
          }, 200);
        });
      }
    }

    var addDeleteWrapper = forms[i].querySelector(
      ".form-field-container-add-delete"
    );
    var addUsersButton = forms[i].querySelector(".add-attendees");

    if (addDeleteWrapper !== null && addUsersButton !== null) {
      var addUsersButtonParent = addUsersButton.parentElement;
      var extraAttendeesWrapper = forms[i].querySelector(
        ".form-group-attendees"
      );

      // var deleteButton = addDeleteWrapper.querySelector(".add-delete__delete");
      // var addButton = addDeleteWrapper.querySelector(".add-delete__add");
      var counter = 0;

      function listenerRemoveFn(evt) {
        var thisDeleteWrapper = evt.currentTarget.closest(
          ".form-field-container-add-delete"
        );
        thisDeleteWrapper.remove();
        counter--;
        checkCount();
        updateAttendeesCount();
      }

      function listenerAddFn() {
        console.log(parseInt($thisForm.getAttribute("data-max-counter")));
        if (counter < parseInt($thisForm.getAttribute("data-max-counter"))) {
          var sourceNode = addDeleteWrapper;
          var node = duplicateNode(sourceNode, ["id", "name"]);
          // node = node.removeAttribute('style');

          sourceNode.parentNode.appendChild(node);

          var nameField = node.querySelector(".form-field__input-name");
          nameField.focus();
          var newRemoveButton = node.querySelector(".add-delete__delete");

          newRemoveButton.addEventListener("click", listenerRemoveFn, false);
        }
      }

      addUsersButton.addEventListener("click", listenerAddFn, false);

      function duplicateNode(
        /*DOMNode*/ sourceNode,
        /*Array*/ attributesToBump
      ) {
        counter++;
        var out = sourceNode.cloneNode(true);
        if (out.hasAttribute("id")) {
          out["id"] = bump(out["id"]);
        }
        var nodes = out.getElementsByTagName("*");

        for (var i = 0, len1 = nodes.length; i < len1; i++) {
          var node = nodes[i];
          for (var j = 0, len2 = attributesToBump.length; j < len2; j++) {
            var attribute = attributesToBump[j];
            if (node.hasAttribute(attribute)) {
              node[attribute] = bump(node[attribute]);
              node.setAttribute("required", "");
            }
          }
        }

        function bump(/*String*/ str) {
          return str + "_" + counter;
        }

        checkCount();

        return out;
      }

      function checkCount() {
        if (counter >= parseInt($thisForm.getAttribute("data-max-counter"))) {
          addUsersButtonParent.style.opacity = 0;
          addUsersButtonParent.style.pointerEvents = "none";
        } else {
          addUsersButtonParent.style.opacity = 1;
          addUsersButtonParent.style.pointerEvents = "all";
        }
        if (counter > 0) {
          hasExtraAttendees = true;
        } else {
          hasExtraAttendees = false;
        }
        updatePersons(counter);
      }

      function updateAttendeesCount() {
        var extraAttendees = extraAttendeesWrapper.querySelectorAll(
          ".form-field-container-add-delete:not(:first-child)"
        );

        for (let i = 0; i < extraAttendees.length; i++) {
          var inputName = extraAttendees[i].querySelector(
            ".form-field__input-name"
          );
          var inputLastname = extraAttendees[i].querySelector(
            ".form-field__input-lastname"
          );

          var correctCount = i + 1;
          inputName.setAttribute(
            "name",
            "extra_deelnemer_voornaam_" + correctCount
          );
          inputLastname.setAttribute(
            "name",
            "extra_deelnemer_achternaam_" + correctCount
          );
        }
      }

      function updatePersons(extraPersons) {
        var numberOfPersons = 1;
        var totalPersons = numberOfPersons + extraPersons;

        $thisForm.querySelector("[name='aantal_personen'").value = totalPersons;
      }
    }

    forms[i].addEventListener("submit", function (event) {
      event.preventDefault();

      // if(hasExtraAttendees === true) {
      //     var getExtraAttendees = extraAttendeesWrapper.querySelectorAll(".form-field-container-add-delete:not(:first-child)");
      //     addHiddenInputs(getExtraAttendees);
      // }

      let $this = this;
      let inputs = this.querySelectorAll(
        '.form-field__input:not([name="extra_deelnemer_voornaam"]):not([name="extra_deelnemer_achternaam"]), .form-field__select'
      );
      let errors = [];
      for (var i = 0; i < inputs.length; i++) {
        inputs[i].classList.remove("error");
        if (validate(inputs[i]).length > 1) {
          errors.push(validate(inputs[i]));
        }
        if (i == inputs.length - 1) {
          if (errors.length == 0) {
            addApplicantHiddenInputs();

            if (hasExtraAttendees === true) {
              var getExtraAttendees = extraAttendeesWrapper.querySelectorAll(
                ".form-field-container-add-delete:not(:first-child)"
              );
              addHiddenInputs(getExtraAttendees);
            }

            var btn = this.querySelector(".submit-form-btn");
            btn.classList.add("submitting");

            grecaptcha.ready(function () {
              var random_token = Math.floor(
                Math.random() * 100000000000000000000
              );
              grecaptcha
                .execute("6LfEc8MZAAAAAD3XnCxGa_et7i9IEwPf9cPS1gWQ", {
                  action: "form_" + random_token,
                })
                .then(function (token) {
                  $this.querySelector("#g-recaptcha-response").value = token;
                  setTimeout(() => {
                    $this.submit();
                  }, 200)
                  
                  // if ($this.querySelector('[name="activiteit_id"') != null) {
                  //   let counter = parseInt(localStorage.getItem("verito-counter") || 1);
                  //   var currentPage = $this.querySelector('[name="activiteit_id"]').value;
                  //   var savedPage = localStorage.getItem("verito-page");

                  //   if (counter > 3) {
                  //     alert("U heeft zich al te vaak aangemeld met dit apparaat");
                  //     setTimeout(() => {
                  //       btn.classList.add("submitting");
                  //     }, 5000);
                  //   } else if (savedPage !== currentPage || savedPage === currentPage && counter <= 3 ) {
                  //     $this.submit();
                  //     localStorage.setItem("verito-page", currentPage);
                  //     localStorage.setItem("verito-counter", counter + 1);
                  //   } else {
                  //     alert("U heeft zich al te vaak aangemeld met dit apparaat");
                  //     setTimeout(() => {
                  //       btn.classList.add("submitting");
                  //     }, 5000);
                  //   }
                  // } else {
                    
                  // }
                });
            });
          } else {
            var list = $this.querySelector("#form-error-messages");
            list.innerHTML = "";

            for (var i = 0; i < errors.length; i++) {
              errors[i][0].classList.add("error");

              var item = document.createElement("li");
              var fieldError = errors[i][1].replaceAll("_", " ");
              item.appendChild(document.createTextNode(fieldError));
              list.appendChild(item);
            }
          }
          break;
        }
      }
    });

    function addHiddenInputs(attendees) {
      var extraAttendeeCounter = 2;
      // var allAttendees = 1;

      attendees.forEach((wrapper) => {
        var firstName = wrapper.querySelector(".form-field__input-name").value;
        var lastName = wrapper.querySelector(
          ".form-field__input-lastname"
        ).value;
        var fullName = firstName + " " + lastName;

        var hiddenInputName = "NAAM_" + extraAttendeeCounter + "E_DEELNEMER";

        var hiddenInput = document.createElement("input");
        hiddenInput.setAttribute("type", "hidden");
        hiddenInput.setAttribute("name", hiddenInputName);
        hiddenInput.setAttribute("value", fullName);
        $thisForm.appendChild(hiddenInput);

        extraAttendeeCounter++;
        // allAttendees++;
      });
    }

    function addApplicantHiddenInputs() {
      var personalDataWrapper = $thisForm.querySelector(
        ".form-group-personal-data"
      );

      if (personalDataWrapper === null) return;

      var firstName = personalDataWrapper.querySelector(
        ".form-field__input-applicant-name"
      ).value;
      var lastName = personalDataWrapper.querySelector(
        ".form-field__input-applicant-lastname"
      ).value;
      var fullName = firstName + " " + lastName;

      var hiddenInputName = "NAAM_1E_DEELNEMER";

      var hiddenInput = document.createElement("input");
      hiddenInput.setAttribute("type", "hidden");
      hiddenInput.setAttribute("name", hiddenInputName);
      hiddenInput.setAttribute("value", fullName);
      $thisForm.appendChild(hiddenInput);
    }
  }

  function validate(input) {
    let errorArray = [input];
    var inputName;
    if (input.name.includes("default_fields")) {
      inputName = input.name.replace("default_fields[", "").replace("]", "");
    } else {
      inputName = input.name;
    }

    if (input.hasAttribute("required") && input.value == "") {
      errorArray.push(inputName + " is nog niet ingevuld");
    }
    if (input.value != "") {
      let emailRe =
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      let phoneRe = /^\d{10,}$/;
      let ibanRe =
        /^([A-Z]{2}[ '+'\\'+'-]?[0-9]{2})(?=(?:[ '+'\\'+'-]?[A-Z0-9]){9,30}$)((?:[ '+'\\'+'-]?[A-Z0-9]{3,5}){2,7})([ '+'\\'+'-]?[A-Z0-9]{1,3})?$/;
      if (
        input.getAttribute("type") === "email" &&
        emailRe.test(input.value.toLowerCase()) != true
      ) {
        errorArray.push(inputName + " is nog niet correct ingevuld");
      }
      if (
        input.getAttribute("type") === "tel" &&
        phoneRe.test(
          input.value.replace(" ", "").replace("-", "").toLowerCase()
        ) != true
      ) {
        errorArray.push(inputName + " is nog niet correct ingevuld");
      }
      if (
        input.classList.contains("event_space_active") &&
        input.hasAttribute("required") &&
        parseInt(input.value) > parseInt(input.getAttribute("data-free-space"))
      ) {
        document.getElementById("error-message-persons").innerHTML =
          "Helaas, er zijn nog maar " +
          input.getAttribute("data-free-space") +
          " plekken beschikbaar";
        errorArray.push("Helaas, er zijn niet genoeg plekken beschikbaar");
      } else {
        var errorMessage = document.getElementById("error-message-persons");
        if (errorMessage) {
          errorMessage.innerHTML = "";
        }
      }
      if (
        input.classList.contains("form-field__select") &&
        input.hasAttribute("required") &&
        input.options[input.options.selectedIndex].disabled == true
      ) {
        errorArray.push(inputName + " is nog niet geselecteerd");
      }
      if (
        input.classList.contains("form-field_iban") &&
        input.hasAttribute("required") &&
        ibanRe.test(input.value.toUpperCase()) != true
      ) {
        errorArray.push(inputName + " is nog niet correct ingevuld");
      }
    }
    return errorArray;
  }

  function reCaptchaFocus() {
    if (!focused) {
      focused = true;
      var head = document.getElementsByTagName("head")[0];
      var script = document.createElement("script");
      script.type = "text/javascript";
      script.src =
        "https://www.google.com/recaptcha/api.js?render=6LfEc8MZAAAAAD3XnCxGa_et7i9IEwPf9cPS1gWQ";
      head.appendChild(script);
    }
  }
}
export { form };
