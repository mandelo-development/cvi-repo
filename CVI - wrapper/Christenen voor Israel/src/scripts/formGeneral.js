async function formGeneral () {

    var $ = require("../../../config/node_modules/jquery");

    $(document).ready(function () {
    
        $('.next').on('click touch', function(e) {
            e.preventDefault();
            let allSteps = $('.form-step').length;
            let currentStep = $('.form-step.active');
            let currentStepIndex = $(currentStep).data('step');
            let nextStep = parseInt(currentStepIndex) + 1;
        
            if ($(this).hasClass('disabled')) {
        
            } else {
                $('#step-' + currentStepIndex).fadeOut('fast', function() {
                    $(this).removeClass('active');
                    $('#step-' + nextStep).fadeIn().addClass('active');
                });
        
                $('.form-step-button').removeClass('active');
                $('#step-button-' + nextStep).addClass('active');
        
                $(this).closest('form').removeClass('first-step');
            }
        
            if (allSteps === nextStep) {
                $(this).hide();
                $('.last-step-button').show();
            }
        });
        
        $('.prev').on('click touch', function(e) {
            e.preventDefault();
        
            let currentStep = $('.form-step.active');
            let currentStepIndex = $(currentStep).data('step');
            let prevStep = parseInt(currentStepIndex) - 1;
        
            $('#step-' + currentStepIndex).fadeOut('fast', function() {
                $(this).removeClass('active');
                $('#step-' + prevStep).fadeIn().addClass('active');
        
                $('.form-step-button').removeClass('active');
                $('#step-button-' + prevStep).addClass('active');
            });
        
            if (prevStep === 1) {
                $('.next').show();
                $('.last-step-button').hide();
                $(this).closest('form').addClass('first-step');
            }
        });
        
        // $("select[name='frequentie']").change(function(event) {
        //     var value = $(this).val();
        //     var ideal = $('#ideal');
        //     var machtiging = $('#machtiging');
        
        //     if(value === 'maandelijks' || value === 'kwartaal' || value === 'jaarlijks' ) {
        //         $(this).closest('form').find('.soort-machtiging').text('PERIODIEK');
        
        //         $(ideal).parent().hide();
        //         $(machtiging).prop('checked', true);
        //         $(machtiging).parent().removeClass('half').addClass('whole');
        
        //         $('.donate-anonymous').hide();
        //         $("input[name='donate_anonymous']").prop('checked', false);
        //         $('.form-group-machtiging').show();
        //     } else {
        //         console.log($(this).closest('form').find('.soort-machtiging'));
        //         $(this).closest('form').find('.soort-machtiging').text('EENMALIG');
        
        //         $(ideal).parent().show();
        //         $(ideal).prop('checked', true);
        //         $(machtiging).prop('checked', false);
        //         $(machtiging).parent().removeClass('whole').addClass('half');
        
        //         $('.donate-anonymous').show();
        //         $('.form-group-machtiging').hide();
        //     }
        
        // });
        $("[name='frequentie']").change(function(event) {
            var value = $(this).val();
            var ideal = $('#ideal');
            var machtiging = $('#machtiging');
        
            if(value === 'maandelijks' || value === 'kwartaal' || value === 'jaarlijks' ) {
                $(this).closest('form').find('.soort-machtiging').text('PERIODIEK');
        
                $(ideal).parent().hide();
                $(machtiging).prop('checked', true);
                $(machtiging).parent().removeClass('half').addClass('whole');
        
                $('.donate-anonymous').hide();
                $("input[name='donate_anonymous']").prop('checked', false);
                $('.form-group-machtiging').show();
            } else {
                console.log($(this).closest('form').find('.soort-machtiging'));
                $(this).closest('form').find('.soort-machtiging').text('EENMALIG');
        
                $(ideal).parent().show();
                $(ideal).prop('checked', true);
                $(machtiging).prop('checked', false);
                $(machtiging).parent().removeClass('whole').addClass('half');
        
                $('.donate-anonymous').show();
                $('.form-group-machtiging').hide();
            }
        
        });
        
        
        $("input[name='custom_bedrag']").focus(function() {
            let inputBedrag = $("input[name='bedrag']");
        
            $(inputBedrag).prop('checked', false);
            $(inputBedrag).prop('required', false);
            $(this).prop('required', true);
            $(this).closest('.enter-amount').addClass('is-focused');
        
        });
        
        $("input[name='bedrag']").click(function() {
            let inputCustomBedrag = $("input[name='custom_bedrag']");
            $(inputCustomBedrag).prop('required', false);
            $(inputCustomBedrag).closest('.enter-amount').removeClass('is-focused');
            $(inputCustomBedrag).val('');
        
            $(this).prop('checked', true);
            $(this).prop('required', true);
        });
        
        
        $("input[name='donate_anonymous']").change(function() {
            if ($(this).is(':checked')) {
                $('.form-group-data').hide();
                $('.form-group-data input').prop('required', false);
                $('.form-group-data select').prop('required', false);
            } else {
                $('.form-group-data').show();
                $('.form-group-data input:not(".form-field__toevoeging")').prop('required', true);
                $('.form-group-data select').prop('required', true);
            }
        });
        
        if ($("input[name='betaalmethode']")) {
            $("input[name='betaalmethode']").change(function() {
                let iban = $("input[name='iban']");
                console.log($(this).val());
                if ($(this).val() === 'machtiging') {
                    $('.donate-anonymous').hide();
                    $("input[name='donate_anonymous']").prop('checked', false);
                    $('.form-group-data').show();
                    $('.form-group-machtiging').show();
                    $(iban).prop('required', true);
                } else {
                    $('.donate-anonymous').show();
                    $('.form-group-machtiging').hide();
                    $(iban).prop('required', false);
                }
            });
        }
        if ($("select[name='land']")) {
            $("select[name='land']").change(function() {
                let bic = $("input[name='bic']");
                var betaalmethode = $("input[name='betaalmethode']:checked");
                if ($(this).val() !== 'NL' && $(betaalmethode).val() !== "ideal") {
                    $(bic).show();
                    $(bic).prop('required', true);
                } else {
                    $("input[name='bic']").hide();
                    $(bic).prop('required', false);
                }
            });
        }

        $('.submit-contact-form-btn').click(function (e) {
            this.classList.add('submitting');
        });
    });
 } export {
     formGeneral
 }