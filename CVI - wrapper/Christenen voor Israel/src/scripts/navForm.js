async function navForms() {
    
    var navForms = document.querySelectorAll('.plate--element.plate--element__nav_form form');
    var veritoPrev = 'form_message[content][verito_field_id_';
    for (var i = 0; i < navForms.length; i++) {
        var formResult = [];
        var formInfo = [];
        navForms[i].addEventListener('submit', function(event) {
            formInfo['form_to'] = this.getAttribute('data-to');
            formInfo['form_verito_id'] = this.getAttribute('data-verito-id');
            formInfo['form_webform_template'] = this.getAttribute('data-webform-template');
            formInfo['form_url'] = this.getAttribute('data-url');
            var formName = this.getAttribute('name');
            event.preventDefault();
            var fieldCounter = 0;
            var resultDiv = document.querySelector('#nav_form-' + this.getAttribute('data-nav-id'));
            var fields = this.querySelectorAll('.form-field__input:not(.form-field-info)');
            var fieldsRadio = this.querySelectorAll('.form-field__radio:not(.form-field-info)');
            var isCheckedRadio = false;
            var fieldsSelect = this.querySelectorAll('.form-field__select:not(.form-field-info)');
            var fieldsCheckbox = this.querySelectorAll('.form-field__checkbox:not(.form-field-info)');
            var isCheckedCheckbox = false;
            var fieldsInfo = this.querySelectorAll('input.form-field-info, select.form-field-info');
            var totalLength = fields.length + fieldsRadio.length + fieldsSelect.length + fieldsCheckbox.length + fieldsInfo.length;
            var validationArray = [];
            for (var z = 0; z < fieldsInfo.length; z++) {
                fieldCounter++;

                if (fieldsInfo[z].parentElement.getAttribute('data-required') == 'true') {
                    console.log(fieldsInfo[z]);
                    validateForm(formName, fieldsInfo[z].name, fieldsInfo[z].type);
                }

                handleNavInputs(fieldsInfo[z].value, fieldsInfo[z].getAttribute('name'), fieldCounter, totalLength, resultDiv);
            }
            for (var j = 0; j < fields.length; j++) {
                fieldCounter++;
                var targetId = fields[j].getAttribute('data_id');
                var value = fields[j].parentElement.getAttribute('data-verito-field-id');
                var name = veritoPrev + fields[j].value + ']';

                if (fields[j].parentElement.getAttribute('data-required') == 'true') {
                    validateForm(formName, fields[j].name, fields[j].type);
                }

                handleNavInputs(value, name, fieldCounter, totalLength, resultDiv);
            }
            for (var k = 0; k < fieldsRadio.length; k++) {
                fieldCounter++;
                var thisRadio = fieldsRadio[k];
                var radioInputs = thisRadio.querySelectorAll('input');

                for (var r = 0; r < radioInputs.length; r++) {
                    if (radioInputs[r].checked) {
                        isCheckedRadio = true;
                        var radioValue = radioInputs[r].value;
                        var radioName = radioInputs[r].getAttribute('name');
                        handleNavInputs(radioValue, radioName, fieldCounter, totalLength, resultDiv);
                    }
                    if ((r + 1) == radioInputs.length) {
                        if (thisRadio.getAttribute('data-required') == 'true' && isCheckedRadio === false) {
                            console.log('is required');
                            validateForm(formName, thisRadio, 'radio');
                        }
                    }
                }
            }
            
            for (var l = 0; l < fieldsSelect.length; l++) {
                fieldCounter++;
                var thisSelect = fieldsSelect[l];
                var selectValue = thisSelect.value;
                var selectName = thisSelect.options[thisSelect.selectedIndex].getAttribute('name');

                if (fieldsSelect[l].parentElement.getAttribute('data-required') == 'true') {
                    validateForm(formName, fieldsSelect[l].name, fieldsSelect[l].type);
                }

                handleNavInputs(selectValue, selectName, fieldCounter, totalLength, resultDiv);
            }
            for (var m = 0; m < fieldsCheckbox.length; m++) {
                fieldCounter++;
                var thisCheckbox = fieldsCheckbox[m];
                var checkboxInputs = thisCheckbox.querySelectorAll('input');

                for (var n = 0; n < checkboxInputs.length; n++) {
                    if (checkboxInputs[n].checked) {
                        isCheckedCheckbox = true;
                        var checkValue = checkboxInputs[n].value;
                        var checkName = checkboxInputs[n].getAttribute('name');
                        handleNavInputs(checkValue, checkName, fieldCounter, totalLength, resultDiv);
                    }
                    if ((n + 1) == checkboxInputs.length) {
                        if (thisCheckbox.getAttribute('data-required') == 'true' && isCheckedCheckbox === false) {
                            console.log('is required');
                            validateForm(formName, thisCheckbox, 'checkbox');
                        }
                    }
                }
            }

            function validateForm(formName, fieldName, inputType) {
                console.log('document.forms[' + formName + '][' + fieldName + '].value');
                console.log(inputType);

                if (inputType == "text") {
                    var x = document.forms[formName][fieldName];
                    var inputValue = x.value;
                    if (inputValue == "") {
                        validationArray.push(x);
                    }
                }
                if (inputType == "select-one") {
                    var z = document.forms[formName][fieldName];
                    var selectValue = z.options[z.selectedIndex].value;
                    console.log({
                        selectValue
                    });
                    if (selectValue == "disabled") {
                        validationArray.push(z);
                    }
                }
                if (inputType == "checkbox") {
                    validationArray.push(fieldName);
                }
                if (inputType == "radio") {
                    validationArray.push(fieldName);
                }
            }

            function handleNavInputs(value, name, index, totalLength, resultDiv) {
                console.log(name + ': ' + value);
                console.log(index + ' === ' + totalLength);
                formResult[name] = value;
                if (index === totalLength - 1) {
                    setTimeout(function() {
                        console.log(validationArray);
                        if (validationArray.length == 0) {
                            generateForm(formResult, formInfo, resultDiv);
                        } else {
                            document.querySelectorAll('.form-field__error').forEach(function(a) {
                                a.classList.remove('form-field__error');
                            });
                            generateErrorList(validationArray);
                        }
                    }, 100);

                }
            }

        });


    }

    function generateForm(data, dataInfo, resultDiv) {
        console.log(data);
        console.log(dataInfo);
        var textInfo = [];
        var j;
        for (j in dataInfo) {
            if (dataInfo.hasOwnProperty(j)) {
                textInfo.push(j + "=" + encodeURIComponent(dataInfo[j]));
            }
        }
        var text = [];
        var i;
        for (i in data) {
            if (data.hasOwnProperty(i)) {
                text.push(i + "=" + encodeURIComponent(data[i]));
            }
        }
        text = text.join("|");
        textInfo = textInfo.join("&");
        console.log(text);
        inputTekst = 'fields=' + text + '&' + textInfo;
        var url = "/form?" + inputTekst;
        var pushStateUrl = "?" + inputTekst;

        // window.history.pushState(null, null, pushStateUrl);

        var xhr = window.XMLHttpRequest ? new XMLHttpRequest : window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : (alert("Browser does not support XMLHTTP."), false);
        xhr.onreadystatechange = text;
        xhr.open("GET", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(text);
        xhr.onload = function() {
            resultDiv.innerHTML = xhr.response;
            console.log(xhr.response);
            setTimeout(function() {
                resultDiv.querySelector('form').submit();
                // console.log('submit');
            }, 200);
        };
        xhr.onerror = function() {
            alert("error");
        };
    }

    function generateErrorList(elementArray) {
        var placeholderArray = [];
        for (var y = 0; y < elementArray.length; y++) {
            var placeholder = elementArray[y].placeholder;
            elementArray[y].classList.add('form-field__error');
            if (placeholder === undefined) {
                placeholderArray.push(elementArray[y].getAttribute('data-placeholder'));
            } else {
                placeholderArray.push(elementArray[y].placeholder);
            }
        }

        if (placeholderArray) {
            document.getElementById('nav-form__errors').innerHTML = "";
            var ul = document.createElement('ul');
            var tag = document.createElement("p");
            var text = document.createTextNode("Er ging iets fout bij het versturen van het bericht.");
            tag.appendChild(text);
            document.getElementById('nav-form__errors').appendChild(tag);
            document.getElementById('nav-form__errors').appendChild(ul);

            placeholderArray.forEach(function(item) {
                let li = document.createElement('li');
                ul.appendChild(li);

                li.innerHTML += item + ' moet worden opgegeven';
            });
        }
    }

 } export {
    navForms
 }