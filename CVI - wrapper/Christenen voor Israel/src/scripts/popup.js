import { gsap } from "../../../config/node_modules/gsap";
import {
   Swiper,
   Navigation,
   Pagination,
   Scrollbar,
   Controller,
   Lazy,
   Mousewheel
} from '../../../config/node_modules/swiper/swiper.esm.js';
Swiper.use([Navigation, Pagination, Scrollbar, Controller, Lazy, Mousewheel]);


let main = document.querySelector("main");
let popupTriggers = [];
var currentPopup;
async function initPopups(thisContainer) {
  popupTriggers = Array.from(document.querySelectorAll(".popup-trigger"));
  var popup = document.querySelector(".popup");

  popupTriggers.forEach((popupTrigger) => {
    // else {
    //    popup = thisContainer.querySelector(".popup--standard");
    // }

    popupTrigger.addEventListener("click", (e) => {
      var dataParent = popupTrigger.getAttribute("data-parent");
      var dataId = popupTrigger.getAttribute("data-id");

      // console.log({dataParent})

      if (dataParent === "popup_content") {
        popup = document.querySelector(".popup--content");
      } else if (dataParent === "popup_download") {
        popup = document.querySelector(".popup--download");
      }
 
      currentPopup = new PopUp(e, popup, dataParent, dataId);
    });
  });
}

class PopUp {
  constructor(e, wrapper, parent, id) {
    this.DOM = { el: wrapper };
    this.DOM.selectedPopup = [];
    this.DOM.popup_inner = wrapper.querySelector(".popup__inner");
    this.DOM.popup_content = wrapper.querySelector(".popup__content");
    this.DOM.wrapper = wrapper;
    this.DOM.parent = parent;
    this.DOM.close = wrapper.querySelector(".popup__close");

    this.DOM.selectedPopup[parent] = id;
    this.initEvents();
    
   this.setPopupContent(
      this.DOM.selectedPopup,
      this.DOM.popup_content,
      this.DOM.wrapper
   );
    
  }
  setPopupContent(data, container, wrapper) {
    var text = [];
    var i;
    for (i in data) {
      if (data.hasOwnProperty(i)) {
        text.push(i + "=" + encodeURIComponent(data[i]));
      }
    }
    var textState = [];
    var i;
    for (i in data) {
      if (data.hasOwnProperty(i)) {
        if (i != "page") {
          textState.push(i + "=" + encodeURIComponent(data[i]));
        }
      }
    }
    text = text.join("&");
    textState = textState.join("&");
    var url = "/popups?" + text;
    var pushStateUrl = "?" + textState;

    // window.history.pushState(null, null, pushStateUrl);

    var xhr = window.XMLHttpRequest
      ? new XMLHttpRequest()
      : window.ActiveXObject
      ? new ActiveXObject("Microsoft.XMLHTTP")
      : (alert("Browser does not support XMLHTTP."), false);
    xhr.onreadystatechange = text;
    xhr.open("GET", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(text);
    xhr.onload = function () {
      container.innerHTML = xhr.response;
      currentPopup.open(container);

      var popupSlider = container.querySelector(".slider-container.media");
      if (typeof popupSlider != "undefined" && popupSlider != null) {
         new Swiper(popupSlider, {
            autoplay: false,
            spaceBetween: 0,
            slidesPerView: 1,
            direction: 'horizontal',
            grabCursor: true,
            loop: true,
            lazy: true,
            navigation: {
                nextEl: popupSlider.querySelector('.swiper-button-next'),
                prevEl: popupSlider.querySelector('.swiper-button-prev'),
            },
            mousewheel: {
                forceToAxis: true,
            }
        });
      }
    };
    xhr.onerror = function () {
      alert("error");
    };
  }
  initEvents() {
    var self = this;
    this.closeFn = () => {
      this.close();
    };

    window.addEventListener("click", function (e) {
      if (e.target == self.DOM.wrapper) {
        self.closeFn();
      }
    });
    this.DOM.close.addEventListener("click", this.closeFn);
  }
  open(ctr) {
    document.body.classList.add("lock-scroll");
    this.DOM.wrapper.classList.add("popup--open");
    if (!ctr.querySelector('.content-overlay__image')) {
      this.DOM.wrapper.classList.add("popup--small");
    } else {
      this.DOM.wrapper.classList.remove("popup--small");
    }
    gsap.to(this.DOM.wrapper, {
      autoAlpha: 1,
      pointerEvents: "all",
    });
  }
  close() {
    document.body.classList.remove("lock-scroll");
    gsap.to(this.DOM.wrapper, {
      autoAlpha: 0,
      pointerEvents: "none",
      onComplete: () => {
        if (this.DOM.parent === "popup_video") {
          let parent = this.DOM.popup_inner.querySelector(".popup__video");
          while (parent.firstChild) {
            parent.firstChild.remove();
          }
        }
      },
    });
    this.DOM.wrapper.classList.remove("popup--open");
  }
}

export { initPopups, PopUp };
