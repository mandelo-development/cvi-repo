async function masonryGrid () {
    
    var $ = require("../../../config/node_modules/jquery");
    var Masonry = require('../../../config/node_modules/masonry-layout');
    var imagesLoaded = require('../../../config/node_modules/imagesloaded');
        imagesLoaded.makeJQueryPlugin( $ );
    
    $(document).ready(function() {

        var grid = document.querySelector('.masonry-grid');

        if (grid) {
            var $grid = new Masonry( grid, {
                // disable initial layout
                initLayout: false,
                itemSelector: '.masonry-grid-item',
                columnWidth: '.masonry-grid-item',
                percentPosition: true,
                gutter: 16,
                horizontalOrder: true,
                isAnimated: true,
                animationOptions: {
                    duration: 400,
                    easing: 'linear',
                    queue: false,
                },
            });

            $($grid).imagesLoaded(function() {
                $grid.layout();
            });
        }

    });

 } export {
    masonryGrid
 }