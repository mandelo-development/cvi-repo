async function navigation () {
    
    var $ = require("../../../config/node_modules/jquery");

    $(document).ready(function() {
        $('body').removeClass('no-transitions');

        $('.navbar-toggler').click(function() {
            $('.navigation').toggleClass('menu-open').toggleClass('mega-menu__active');
            $('body').toggleClass('lock-scroll');
        });

        $(".nav-item.search").on('click', function(e) {
            $("#search__popup").addClass('active');
        });
    
        $(".search__popup__close__wrapper").on('click', function(e) {
            $("#search__popup").removeClass('active');
        });

        // Hide Navbar on on scroll down
        var didScroll;
        var lastScrollTop = 0;
        var delta = 0;
        var navbar = $('.navigation');
        var navbarHeight = $(navbar).outerHeight();
        /*var ctaBarMob = $('.cta-bar');*/
    
        setInterval(function() {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);
    
        function hasScrolled() {
    
            let st = $(window).scrollTop();
    
            // Make sure they scroll more than delta
            if (Math.abs(lastScrollTop - st) <= delta)
                return;

            if (navbarHeight > st) {
                $(navbar).addClass('nav-top');
            } else {
                $(navbar).removeClass('nav-top');
            }
    
            // If they scrolled down and are past the navbar, add class .nav-down.
            // This is necessary so you never see what is "behind" the navbar.
            if (st > lastScrollTop) {
                // Scroll Down
                $(navbar).removeClass('nav-up').addClass('nav-down');
                /*$(ctaBarMob).addClass('show-cta');*/
            } else {
                // Scroll Up
                if (st + $(window).height() < $(document).height()) {
                    $(navbar).removeClass('nav-down').addClass('nav-up');
                    /*$(ctaBarMob).removeClass('show-cta');*/
                }
            }
    
            lastScrollTop = st;
        }

        $(window).scroll(function() {
            if ($(window).scrollTop() >= 40) {
                $('.navigation').addClass('fixed-header');
            } else {
                $('.navigation').removeClass('fixed-header');
            }

            didScroll = true;
        });

        var url = location.pathname;
        if($('#extra-menu-bar').length == 0 && url !== '/' ) {
            $('body').addClass('no-submenu');
        } else if (url === '/' ) {
            $('body').addClass('home');
        }

        if ($(window).width() <= 1024) {
            var filter_dropdown = document.getElementsByClassName("mobile-dropdown");
            var i;
            var j;
    
            for (i = 0; i < filter_dropdown.length; i++) {
                filter_dropdown[i].onclick = function() {
    
                    if (this.classList.contains("active")) {
    
                        this.nextElementSibling.style.maxHeight = null;
                        this.classList.remove("active");
    
                    } else {
    
                        for (j = 0; j < filter_dropdown.length; j++) {
                            filter_dropdown[j].nextElementSibling.style.maxHeight = null;
                            filter_dropdown[j].classList.remove("active")
                        }
    
                        this.classList.add("active");
    
                        var panel = this.nextElementSibling;
                        
                        if (panel.style.maxHeight) {
                            panel.style.maxHeight = null;
                        } else {
                            panel.style.maxHeight = panel.scrollHeight + "px";
                        }
    
                    }
    
                }
            }
        }

        if ($(window).width() <= 767) {
    
            $('.nav-item.main-nav .dropdown-arrow').on('click', function(e) {
                let otherMegaMenus = $('.mega-menu');
    
                let hoofdmenuID = $(this).attr('data-hoofdmenu');
                let currentMegaMenu = $('.mega-menu[data-megamenu = ' + hoofdmenuID + ']'),
                    isShowing = currentMegaMenu.is(":visible");
    
                // Hide all
                otherMegaMenus.hide();
    
                // If our menu isn't showing, show it
                if (!isShowing) {
                    currentMegaMenu.show();
                }
    
                e.preventDefault();
            });
    
            $('.nav-item .sub-dropdown-arrow').on('click', function(e) {
                let otherSubMenus = $('.dropdown-menu');
    
                let submenuID = $(this).attr('data-id');
                let currentSubMenu = $('.dropdown-menu[data-id = ' + submenuID + ']'),
                    isShowing = currentSubMenu.is(":visible");
    
                // Hide all
                otherSubMenus.hide();
    
                // If our menu isn't showing, show it
                if (!isShowing) {
                    currentSubMenu.show();
                }
    
                e.preventDefault();
            });
    
            

            var section = $(".section-container");
    
            section.each(function (i, element) {
                var mobileSpaceAbove = parseInt($(element).attr('data-boven'));
                var mobileSpaceUnder = parseInt($(element).attr('data-onder'));
    
                if(isNaN(mobileSpaceAbove)) {
                    var padTop = parseInt($(element).css('padding-top'));
                    var newPadTop = padTop / 2;
    
                    $(element).css({'padding-top': newPadTop + 'px'});
                } else {
                    $(element).css({'padding-top': mobileSpaceAbove + 'px'});
                }
    
                if(isNaN(mobileSpaceUnder)) {
                    var padBot = parseInt($(element).css('padding-bottom'));
                    var newPadBot = padBot / 2;
    
                    $(element).css({'padding-bottom': newPadBot + 'px'});
                } else {
                    $(element).css({'padding-bottom': mobileSpaceUnder + 'px'});
                }
            });
    
            var tussenruimte = $(".tussenruimte");
    
            tussenruimte.each(function(i, element) {
                var padTop = parseInt($(this).css('padding-top'));
                var padBot = parseInt($(this).css('padding-bottom'));
    
                var newPadTop = padTop / 1.4;
                var newPadBot = padBot / 1.4;
    
                $(this).css({
                    'padding-top': newPadTop + 'px'
                });
                $(this).css({
                    'padding-bottom': newPadBot + 'px'
                });
            });
    
        } else {
            $('.nav-item.main-nav').on('mouseenter', function() {
                var nav = $('.navigation');
                var megaMenuWrapper = $('.mega-menu-wrapper');
                var otherMegaMenus = $('.mega-menu');
    
                $('.nav-item.main-nav').removeClass('hovered-nav-item');
                $(this).addClass('hovered-nav-item');
    
                if ($(this).hasClass('dropdown')) {
                    let hoofdmenuID = $(this).children('.nav-link').attr('data-hoofdmenu');
                    let currentMegaMenu = $('.mega-menu[data-megamenu = ' + hoofdmenuID + ']');
    
                    $(nav).addClass('mega-menu__active');
                    $('body').addClass('mega-menu__overlay');
    
                    let currentMegaMenuHeight = $(currentMegaMenu).outerHeight(true);
                    $(megaMenuWrapper).css("height", currentMegaMenuHeight + 'px');
    
                    $(otherMegaMenus).removeClass('active');
                    $(currentMegaMenu).addClass('active');
                } else {
                    $(otherMegaMenus).removeClass('active');
    
                    $(nav).removeClass('mega-menu__active');
                    $('body').removeClass('mega-menu__overlay');
                }
            });
    
            $('.navigation').on('mouseleave', function() {
                var nav = $('.navigation');
                var otherMegaMenus = $('.mega-menu');
    
                $('.nav-item.main-nav').removeClass('hovered-nav-item');
    
                $(otherMegaMenus).removeClass('active');
                $(nav).removeClass('mega-menu__active');
                $('body').removeClass('mega-menu__overlay');
            });

        }

        

    });

 } export {
    navigation
 }