async function geschiedenis () {
    var $ = require("../../../config/node_modules/jquery");

    $(document).ready(function() {

        var $root = $('html, body');

        $('a[href^="#"]:not(.tab-links a)').click(function() {
            $root.animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 500);
    
            return false;
        });

        /*STICKY ELEMENT*/
        var stickyElement = $('.timeline-container');

        if ($(stickyElement).length) {
            var intersectionOptions = {
                root: null,
                rootMargin: '0px 0px -50% 0px',
                threshold: 0.9,
            };

            function intersectionCallbackConts(entries, observer) {
                $(entries).each(function(i, entry) {
                    let elem = entry.target;
                    if (entry.intersectionRatio > intersectionOptions.threshold) {
                        $(anchorPoints).removeClass('active');
                        $(elem).addClass('active');

                        var id = $(elem).attr('id');
                        $('.year a').removeClass('active');
                        $('.year a[data-id=' + id + ']').addClass('active');
                    } else {}
                });
            }

            var observer = new IntersectionObserver(intersectionCallbackConts, intersectionOptions);

            $(stickyElement).closest('.section-container').css("overflow", "visible");

            var anchorPoints = document.querySelectorAll('.anchor');

            $(anchorPoints).each(function(i, element) {
                observer.observe(element);
            });
        }

    });

 } export {
    geschiedenis
 }