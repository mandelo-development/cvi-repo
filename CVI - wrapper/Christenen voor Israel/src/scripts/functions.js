async function functions () {
    
    if (typeof(document.querySelector('.print')) != 'undefined' && document.querySelector('.print') != null) {
        document.querySelector('.print').addEventListener('click', function() {
            window.print();
        });
    }
    
    var $ = require("../../../config/node_modules/jquery");

    const debounce = function(func, wait, immediate) {
        var timeout;
        return function() {
            var context = this,
                args = arguments;
            var later = function() {
                timeout = null;
                if (!immediate) func.apply(context, args);
            };
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
            if (callNow) func.apply(context, args);
        };
    };

    $(document).ready(function () {

        /*KENNISBANK DROPDOWN*/

        $('#kennisbank_dropdown').on('change', debounce(function(event) {
            var catId = $(this).val(); // get selected value
            if (catId) { // require a URL
                window.location = '/kennisbank?category=' + catId; // redirect
            }
            return false;
        }, 200));
                
        /*PLAY VIDEO*/

        var videoPlayer = $('.play-overlay');

        $(videoPlayer).on('click', function() {

            var video = $(this).parent('.video-player').find('.video');

            if (video[0].paused === false) {
                video[0].pause();
                $(this).removeClass('hide');
            } else {
                video[0].play();
                $(this).addClass('hide');
            }

            return false;
        });

        /*COPY TO CLIPBOARD*/

        $('.copy-link').on('click', function(e) {
            e.preventDefault();
            var href = $(this).attr('href');
            copyToClipboard(href);
        });

        /*ACCORDION FUNCTION*/

        accordionClick();

    });

    function accordionClick() {

        var acc = document.getElementsByClassName("accordion");
        var i;
        var j;

        for (i = 0; i < acc.length; i++) {
            acc[i].onclick = function() {

                if (this.classList.contains("active")) {

                    this.nextElementSibling.style.maxHeight = null;
                    this.classList.remove("active");

                } else {

                    for (j = 0; j < acc.length; j++) {
                        acc[j].nextElementSibling.style.maxHeight = null;
                        acc[j].classList.remove("active")
                    }

                    this.classList.add("active");

                    var panel = this.nextElementSibling;

                    if (panel.style.maxHeight) {
                        panel.style.maxHeight = null;
                    } else {
                        panel.style.maxHeight = panel.scrollHeight + "px";
                    }

                }

            }
        }
    }

    function copyToClipboard(href) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val(href).select();
        document.execCommand("copy");
        $temp.remove();
    }

} export {
    functions
}