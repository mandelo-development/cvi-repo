async function tabs () {
    var $ = require("../../../config/node_modules/jquery");
    
    $(document).ready(function () {

        /*TABS*/

        $('.tabs-container .tab-links a').on('click', function(e) {
            var currentAttrValue = $(this).attr('href');

            // Show/Hide Tabs
            $('.tabs-container ' + currentAttrValue).addClass('active').siblings().removeClass('active');

            // Change/remove current tab to active
            $(this).parent('li').addClass('active').siblings().removeClass('active');

            e.preventDefault();
        });

        $(window).on('resize load', function() {
            fixHeight('.tabs-wrapper .tab', '.tabs-wrapper');
        });
        
    });

    function fixHeight(elem, wrapper) {
        var maxHeight = 0;
        $(elem).css('height', 'auto');
        $(elem).each(function() {
            if ($(this).height() > maxHeight) {
                maxHeight = $(this).height();
            }
        });
        $(wrapper).height(maxHeight);
    }

 } export {
    tabs
 }