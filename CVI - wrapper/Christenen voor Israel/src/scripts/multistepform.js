async function multiform () {
    var $ = require("../../../config/node_modules/jquery");
    $('.form-multi [data-step]').on('click touch', function(e) {
        e.preventDefault();
        
        let clickedStepIndex = $(e.currentTarget).data('step');
        let allSteps = $('.step-form__step').length;
        let currentStep = $('.step-form__step.active');
        let currentStepIndex = $(currentStep).data('step-group');
        let prevOrNextStep;
        let forwards = false;
        let backwards = false;
        
        if (clickedStepIndex !== currentStepIndex) {
            var stepDifference = Math.abs(clickedStepIndex - currentStepIndex);
            if (clickedStepIndex > currentStepIndex) {
                forwards = true;
                backwards = false;
                prevOrNextStep = parseInt(currentStepIndex) + stepDifference;
            } else if (clickedStepIndex < currentStepIndex) {
                forwards = false;
                backwards = true;
                prevOrNextStep = parseInt(currentStepIndex) - stepDifference;
            }

            // console.log({forwards}, {backwards}, {prevOrNextStep}, {allSteps}, {currentStep}, {currentStepIndex} )
        
            $('[data-step-group="' + currentStepIndex + '"]').fadeOut('fast', function() {
                $(this).removeClass('active');
                // console.log($('.form-group-indicator-button[data-step="' + prevOrNextStep + '"]').parent().siblings().find('.form-group-indicator-button-active').removeClass('form-group-indicator-button-active'));
                $('[data-step-group="' + prevOrNextStep + '"]').fadeIn(500).addClass('active');
                $('.form-group-indicator-button[data-step="' + prevOrNextStep + '"]').addClass('form-group-indicator-button-active');
            });
        }
    });
    

    var getSiblings = function (elem, choise) {
        var siblings = [];
        var sibling = elem.parentNode.firstElementChild;
        while (sibling) {
            if (sibling.nodeType === 1 && sibling !== elem) {
                if (choise === true) {
                    if (sibling.classList.contains('sales-fields')) {
                        siblings.push(sibling);
                    }
                } else {
                    siblings.push(sibling);
                }
            }
            sibling = sibling.nextSibling
        } return siblings;
    };
    
    var getPreviousSiblings = function (elem, filter) {
        var sibs = [];
        while (elem = elem.previousSibling) {
            if (elem.nodeType === 3) continue; // text node
            if (!filter || filter(elem)) sibs.push(elem);
        }
        return sibs;
    }
} export {
    multiform
}