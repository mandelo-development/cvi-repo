async function filter () {

	var $ = require("../../../config/node_modules/jquery");

	$(document).ready(function () {

		const debounce = function(func, wait, immediate) {
			var timeout;
			return function() {
				var context = this,
					args = arguments;
				var later = function() {
					timeout = null;
					if (!immediate) func.apply(context, args);
				};
				var callNow = immediate && !timeout;
				clearTimeout(timeout);
				timeout = setTimeout(later, wait);
				if (callNow) func.apply(context, args);
			};
		};

		var selectedFilters = [];
		selectedFilters['page'] = 1;
		var textFilter = document.getElementById("text__filter__input");
		var taxonmyFilters = document.querySelectorAll(".filter-button");
		var dateFilterFrom = document.getElementById("date__filter__from");
		var dateFilterTo = document.getElementById("date__filter__to");
		var sortFilter = document.getElementById("sort__filter");
		var sprekerFilter = document.getElementById("spreker__filter__input");
		var locationFilter = document.getElementById("location__filter__input");
		var radiusFilter = document.getElementById("radius__filter__input");
		var locationFilterButton = document.getElementById("location__filter__button");
		var postType;
		var theEnd = false;
		var appended = false;
		var container = document.getElementById("result__wrapper");
		var i = 0;

		if ($(container).length > 0) {

			function makeRequest(data, type) {
				var text = [];
				var i;
				for (i in data) {
					if (data.hasOwnProperty(i)) {
						text.push(i + "=" + encodeURIComponent(data[i]));
					}
				}
				var textState = [];
				var i;
				for (i in data) {
					if (data.hasOwnProperty(i)) {
						if (i != 'page') {
							textState.push(i + "=" + encodeURIComponent(data[i]));
						}
					}
				}
				text = text.join("&");
				textState = textState.join("&");
				var url = "/filters?" + text;
				var pushStateUrl = "?" + textState;
				var loader = "<div class='loader'></div>";

				window.history.pushState(null, null, pushStateUrl);

				var xhr = window.XMLHttpRequest ? new XMLHttpRequest : window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : (alert("Browser does not support XMLHTTP."), false);
				xhr.onreadystatechange = text;
				$(container).append(loader);
				xhr.open("GET", url, true);
				xhr.setRequestHeader("Content-Type", "application/json");
				xhr.send(text);
				xhr.onload = function() {
					container.classList.add("fade-out");
					container.classList.remove("has-featured");
					setTimeout(function() {
						if (type == 'append') {
							container.innerHTML = container.innerHTML + xhr.response;
							if (!xhr.response.includes('+==THEEND==+')) {
								setTimeout(function() {
									appended = false;
								}, 1000);
							} else {
								theEnd = true;
							}
						} else {
							container.innerHTML = xhr.response;
						}
						setTimeout(function() {
							$('.loader').remove();
							container.classList.remove('fade-out');
							container.classList.remove("nth-container");
						}, 50);
					}, 100);
				};
				xhr.onerror = function() {
					alert("error");
				};

			}
			if (document.querySelector('body').classList.contains('paginate_index')) {
				document.addEventListener('scroll', function() {
					// console.log(appended);
					// console.log(theEnd);
					let resultElement = document.querySelector('#result__wrapper');
					let position = elementInViewport(resultElement, resultElement.offsetTop, resultElement.offsetHeight, 100);
					if (position && !appended && !theEnd) {
						appended = true;
						selectedFilters['page'] = selectedFilters['page'] + 1;
						makeRequest(selectedFilters, 'append');
					}
				})
			}

			if(window.location.pathname == '/podcasts') {
				selectedFilters.data_object = 'podcast';
			}
			if(window.location.pathname == '/producten' || window.location.pathname == '/boeken') {
				selectedFilters.data_object = 'product';
			}

			for (; i < taxonmyFilters.length; i++) {
				postType = taxonmyFilters[i].getAttribute("data-object");
				selectedFilters.data_object = postType;
				taxonmyFilters[i].addEventListener("click", debounce(function(e) {
					selectedFilters['page'] = 1;
					appended = false;
					theEnd = false;

					$(taxonmyFilters).removeClass('active');
					$(this).addClass('active');

					var parentTax = this.getAttribute("data-parent");
					var buttonId = this.getAttribute("value");
					selectedFilters[parentTax] = buttonId;

					makeRequest(selectedFilters);
				}, 100));
			}

			if (textFilter !== null) {
				textFilter.addEventListener('keyup', debounce(function(event) {
					selectedFilters['page'] = 1;
					var parentTax = this.getAttribute("data-parent");
					var keyUpValue = this.value;
					appended = false;
					theEnd = false;

					postType = this.getAttribute("data-object");
					selectedFilters[parentTax] = keyUpValue;
					selectedFilters.data_object = postType;

					makeRequest(selectedFilters);
				}, 250));
			}

			if (dateFilterFrom !== null) {
				dateFilterFrom.addEventListener("change", debounce(function(event) {
					selectedFilters['page'] = 1;
					var parentTax = this.getAttribute("data-parent");
					var dateValue = this.value;
					appended = false;
					theEnd = false;

					postType = this.getAttribute("data-object");
					selectedFilters[parentTax] = dateValue;
					selectedFilters.data_object = postType;

					makeRequest(selectedFilters);
				}, 250));
			}

			if (dateFilterTo !== null) {
				dateFilterTo.addEventListener('change', debounce(function(event) {
					selectedFilters['page'] = 1;
					var parentTax = this.getAttribute("data-parent");
					var dateValue = this.value;
					appended = false;
					theEnd = false;

					postType = this.getAttribute("data-object");
					selectedFilters[parentTax] = dateValue;
					selectedFilters.data_object = postType;

					makeRequest(selectedFilters);
				}, 250));
			}

			if (sortFilter !== null) {
				sortFilter.addEventListener('change', debounce(function(event) {
					selectedFilters['page'] = 1;
					appended = false;
					theEnd = false;
					var parentTax = this.getAttribute("data-parent");
					var sortValue = this.options[this.selectedIndex].value;

					postType = this.getAttribute("data-object");
					selectedFilters[parentTax] = sortValue;
					selectedFilters.data_object = postType;

					makeRequest(selectedFilters);
				}, 250));
			}

			if (locationFilter !== null) {
				locationFilterButton.addEventListener('click', debounce(function(event) {
					selectedFilters['page'] = 1;
					appended = false;
					theEnd = false;
					//spreker
					var parentSpreker = sprekerFilter.getAttribute("data-parent");
					var valueSpreker = sprekerFilter.value;

					//location
					var parentLoc = locationFilter.getAttribute("data-parent");
					var valueLoc = locationFilter.value;

					//radius
					var parentRad = radiusFilter.getAttribute("data-parent");
					var valueRad = radiusFilter.value;

					postType = this.getAttribute("data-object");
					selectedFilters[parentSpreker] = valueSpreker;
					selectedFilters[parentLoc] = valueLoc;
					selectedFilters[parentRad] = valueRad;
					selectedFilters.data_object = postType;

					makeRequest(selectedFilters);
				}, 250));
			}

			$(function() {
				var paramFilters = [];
				// Filter based on parameters in URL
				var param_object = getUrlParameter('data_object');
				var param_text = getUrlParameter('search_input');
				var param_categories = getUrlParameter('categories');
				var param_date_from = getUrlParameter('date_input_from');
				if (param_categories) {
					selectedFilters['categories'] = param_categories;
				}
				var param_date_to = getUrlParameter('date_input_to');
				var param_sort = getUrlParameter('sort_input');
				var param_spreker = getUrlParameter('spreker_input');
				var param_location = getUrlParameter('location_input');
				var param_radius = getUrlParameter('radius_input');

				if (param_object || param_text || param_categories || param_date_from || param_date_to || param_sort || param_spreker || param_location || param_radius) {

					paramFilters.data_object = param_object;

					if (param_text !== undefined) {
						$('#text__filter__input').val(param_text);

						paramFilters.search_input = param_text;
					}

					if (param_categories !== undefined) {
						var categoriesElement = $('*[value="' + param_categories + '"]');

						paramFilters.categories = param_categories;

						$(taxonmyFilters).removeClass('active');
						$(categoriesElement).addClass('active');
					}

					if (param_date_from !== undefined) {
						$('#date__filter__from').val(param_date_from);

						paramFilters.date_input_from = param_date_from;
					}

					if (param_date_to !== undefined) {
						$('#date__filter__to').val(param_date_to);

						paramFilters.date_input_to = param_date_to;
					}

					if (param_sort !== undefined) {
						$('#sort__filter').val(param_sort);

						paramFilters.sort_input = param_sort;
					}

					if (param_spreker !== undefined) {
						$('#spreker__filter__input').val(param_spreker);

						paramFilters.spreker_input = param_spreker;
					}

					if (param_location !== undefined) {
						$('#location__filter__input').val(param_location);

						paramFilters.location_input = param_location;
					}

					if (param_radius !== undefined) {
						$('#radius__filter__input').val(param_radius);

						paramFilters.radius_input = param_radius;
					}

					makeRequest(paramFilters);
				}

			});

			// Get parameters from URL
			function getUrlParameter(sParam) {
				var sPageURL = window.location.search.substring(1),
					sURLVariables = sPageURL.split('&'),
					sParameterName,
					i;

				for (i = 0; i < sURLVariables.length; i++) {
					sParameterName = sURLVariables[i].split('=');

					if (sParameterName[0] === sParam) {
						return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
					} else {}
				}
			}
		}
	});

	function elementInViewport(el, top, height, offset) {

		while (el.offsetParent) {
			el = el.offsetParent;
			top += el.offsetTop;
		}

		return (
			(window.pageYOffset + window.innerHeight) > (height + top - offset)
		);
	}

} export {
	filter
}