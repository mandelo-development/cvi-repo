async function search () {

    let searchForms = document.querySelectorAll('.search_form--parent');

    for (var i = 0; i < searchForms.length; i++) {
        let searchForm = searchForms[i];
        let searchFormInput = searchForms[i].querySelector(".search_form");

        searchForm.addEventListener("submit", function(e) {
            e.preventDefault();

            var searchPage = searchFormInput.getAttribute('data-search');
            var formName = searchFormInput.name;
            var searchQuery = searchFormInput.value;

            var finalUrl = searchPage + '?' + formName + '=' + searchQuery;

            window.location.href = finalUrl;
        });
    }
    // function getSearchResult(inputValue) {
    //     let request_method = 'GET';
    //     let variables = [];
    //     let post_url = '/zoekresultaten?q=' + inputValue + '&show_layout=false';
    //     function ajaxReq() {
    //         if (window.XMLHttpRequest) {
    //             return new XMLHttpRequest();
    //         } else if (window.ActiveXObject) {
    //             return new ActiveXObject("Microsoft.XMLHTTP");
    //         } else {
    //             alert("Browser does not support XMLHTTP.");
    //             return false;
    //         }
    //     }
    //     var xmlhttp = ajaxReq();
    //     xmlhttp.onreadystatechange = variables;
    //     xmlhttp.open(request_method, post_url, true); // set true for async, false for sync request
    //     xmlhttp.setRequestHeader('Content-Type', 'application/json');
    //     xmlhttp.send(variables); // or null, if no parameters are passed
    //     xmlhttp.onload = function () {
    //         let resultsWrapper = document.getElementById('search_results');
    //         resultsWrapper.innerHTML = xmlhttp.response;
    //         resultsWrapper.classList.remove("is-loading");
    //     };
    //     xmlhttp.onerror = function () {
    //         alert('error');
    //     };
    // }
} export {
    search
}
