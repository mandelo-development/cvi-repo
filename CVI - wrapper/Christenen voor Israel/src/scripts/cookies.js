async function cookies () {

    var $ = require("../../../config/node_modules/jquery");

    $(document).ready(function() {
        $("#close-notification").click(function () {
            $('body').removeClass('open-notification');
            createCookie('melding', 1, 0);
        });

        if (readCookie('melding') === null) {
            $('body').addClass('open-notification');
        } else {
            $('body').removeClass('open-notification');
        }
    });

    function createCookie(name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        } else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    }
    
    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }
    
 } export {
    cookies
 }