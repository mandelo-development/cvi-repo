async function youtube () {

    var $ = require("../../../config/node_modules/jquery");

    $(document).ready(function () {
        
        var YoutubeVideos = (function() {
            // VARIABLES
            var yt_players = {},    // players array (to coltrol players individually)
                queue = [];       // videos queue (once api is ready, transform this into YT player)

            // Constructor
            function YoutubeVideos() {}

            // METHODS
            // Add elements to queue
            YoutubeVideos.prototype.add = function($video) {
                queue.push($video);
            };

            // Load YT API
            YoutubeVideos.prototype.loadApi = function() {
                // jQuery get script
                $.getScript("https://www.youtube.com/iframe_api", function() {
                    // once loaded, create the onYouTubeIframeAPIReady function
                    window.onYouTubeIframeAPIReady = function() {
                        queue.forEach(function($video) {
                            // Create the YT player
                            var player = new YT.Player($video.find(".youtubePlayer")[0], {
                                'width': "100%",
                                'height': "100%",
                                'videoId': $video.data("youtube-id"),
                                'playerVars': { 'controls': 1, 'rel': 0, 'showinfo': 0, 'modestbranding': 1, 'info': 0, 'origin': window.location.href },
                                'events': {
                                    'onChange': "afterChange",
                                }
                            });
                            // console.log(player);
                            // add to players array
                            yt_players[$video.data("object-id")] = player
                        });
                    };
                });

                return yt_players;
            };

            return YoutubeVideos;

        })();

        // Initialize Youtube players

        var youtube_videos = new YoutubeVideos();

        $('.video-wrapper.youtube-video-wrapper').each(function() {
            youtube_videos.add($(this));
        });
        
        var youtube_players = youtube_videos.loadApi();

        // Youtube video actions
        $('.video-wrapper.youtube-video-wrapper .video_placeholder').click(function() {
            var object_id = $(this).parents(".youtube-video-wrapper").data("object-id");
            var youtube_player = youtube_players[object_id];
            $(this).parents(".video-wrapper").find(".video-container").show();
            youtube_player.playVideo();
        });

        $('.video-wrapper.youtube-video-wrapper .close-video').click(function() {
            var object_id = $(this).parents(".youtube-video-wrapper").data("object-id");
            var youtube_player = youtube_players[object_id];
            $(this).parents(".video-wrapper").find(".video-container").hide();
            youtube_player.stopVideo();
        });

        if ($(".highlighted-video .youtube-video-wrapper")[0]) {
            function onPlayerReady(event) {
                event.target.mute();
                event.target.playVideo();
            }
        }
    });
} export {
    youtube
}