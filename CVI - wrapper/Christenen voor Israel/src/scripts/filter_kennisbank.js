async function kennisbank () {

	const debounce = function(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this,
				args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	};	
		
	let knowledgeBase = document.querySelectorAll('.kennisbank-container:not(.products-container)')[0];
	if (knowledgeBase) {
		let knowledgeBaseResult = document.getElementById("kennisbank__result__wrapper");
		let knowledgeBaseFilters = knowledgeBase.querySelectorAll('.filter-input input');
		let mainCategories = knowledgeBase.querySelectorAll('.filter-button-big');
		let selectFilters = knowledgeBase.querySelectorAll('.select__filter select');
		let subcatFilter = document.querySelectorAll('.subcat__filter__input');
		var textFilterKennisbank = document.getElementById("kennisbank__text__filter__input");
		let parameters = [];
		parameters['page'] = 1;
		var theEnd = false;
		var appended = false;

		if (textFilterKennisbank !== null) {
			textFilterKennisbank.addEventListener('keyup', debounce(function(event) {
			
				var postType = this.getAttribute("data-object");
				var parent = this.getAttribute("data-parent");
				var keyUpValue = this.value;
				parameters['search'] = keyUpValue;
				parameters['page'] = 1;
				theEnd = false;
				appended = false;

				let filterForPrev = '';
				getSelected(knowledgeBaseFilters, filterForPrev);
				filterCategory(true);

			}, 250));
		}

		mainCategories.forEach((category, i) => {
			category.addEventListener('click', function() {
				mainCategories.forEach((cat, j) => {
					cat.classList.remove('active');
				});
				category.classList.add('active');
				let categoryId = this.getAttribute('data-value');
				let inputField = document.querySelector('#sub_categories_' + categoryId);
				let subCatList = document.querySelectorAll('.sub_cat_list');
				for (var i = 0; i < subCatList.length; i++) {
					subCatList[i].classList.remove('show');
				}
				if (inputField) {
					inputField.classList.add('show');
				}
				if (inputField && inputField.childElementCount >= 0) {
					document.getElementById('subcat').classList.add('show');
				} else {
					document.getElementById('subcat').classList.remove('show');
				}
				parameters['category'] = categoryId;
				let filterForPrev = '';
				getSelected(knowledgeBaseFilters, filterForPrev);
				filterCategory(true);
			})
		});

		function findCategory() {
			let button = document.querySelectorAll('.filter-button-big.active')[0];
			let categoryId = button.getAttribute('data-value');
			parameters['category'] = categoryId;
		}

		function clickFilter(items) {
			let filterForPrev = '';
			findCategory();
			items.forEach((filter, i) => {
				filter.addEventListener('click', function() {
					getSelected(items, filterForPrev);
				})
			});
		}

		function getSelected(items, filterForPrev) {
			let newClick = true;
			items.forEach((filterItem, j) => {
				console.log(filterItem)
				let filterFor = filterItem.getAttribute('data-for');
				if (filterForPrev != filterFor) {
					newClick = true;
				}
				filterForPrev = filterFor;
				if (!parameters[filterFor] || newClick) {
					parameters[filterFor] = [];
					newClick = false;
				}
				if (filterItem.checked == true) {
					if (parameters[filterFor].indexOf(filterItem.value) == -1) {
						parameters[filterFor].push(filterItem.value);
					}
				}
				if (j == items.length - 1) {
					filterCategory(true);
				}
			})
		}

		clickFilter(knowledgeBaseFilters);

		selectFilters.forEach((select, i) => {
			select.addEventListener('change', function() {
				let filterFor = this.getAttribute('data-for');
				parameters[filterFor] = this.value;
				filterCategory(false);
				getSelected(knowledgeBaseFilters);
			})
		});

		function filterCategory(resetSub) {
			parameters['page'] = 1;
			theEnd = false;
			appended = false;
			makeRequestKennisbank('/filters-kennisbank', parameters, knowledgeBaseResult, true, false);
		}

		function makeRequestKennisbank(path, data, response, fade, resetSub, type) {
			var text = [];
			var i;
			for (i in data) {
				if (data.hasOwnProperty(i)) {
					text.push(i + "=" + encodeURIComponent(data[i]));
				}
			}
			text = text.join("&");
			var url = path + "?" + text;
			var pushStateUrl = "?" + text;
			window.history.pushState(null, null, pushStateUrl);

			var xhr = window.XMLHttpRequest ? new XMLHttpRequest : window.ActiveXObject ? new ActiveXObject("Microsoft.XMLHTTP") : (alert("Browser does not support XMLHTTP."), false);
			xhr.onreadystatechange = text;
			xhr.open("GET", url, true);
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.send(text);
			xhr.onload = function() {
				if (fade) {
					response.classList.add("fade-out");
				}
				setTimeout(function() {
					if (type == 'append') {
						response.innerHTML = response.innerHTML + xhr.response;
						if (!xhr.response.includes('+==THEEND==+')) {
							setTimeout(function() {
								appended = false;
							}, 1000);
						} else {
							theEnd = true;
						}
					} else {
						response.innerHTML = xhr.response;
					}
					if (fade) {
						setTimeout(function() {
							response.classList.remove("fade-out");
							response.classList.remove("nth-container");
						}, 50);
					}
				}, 100);
			};
			xhr.onerror = function() {
				alert("error");
			};
		}

		function elementInViewport(el, top, height, offset) {

			while (el.offsetParent) {
				el = el.offsetParent;
				top += el.offsetTop;
			}
	
			return (
				(window.pageYOffset + window.innerHeight) > (height + top - offset)
			);
		}

		if (document.querySelector('body').classList.contains('paginate_index')) {
			document.addEventListener('scroll', function() {
				// console.log(appended);
				// console.log(theEnd);
				let resultElement = document.querySelector('#kennisbank__result__wrapper');
				let position = elementInViewport(resultElement, resultElement.offsetTop, resultElement.offsetHeight, 100);
				if (position && !appended && !theEnd) {
					appended = true;
					parameters['page'] = parameters['page'] + 1;
					makeRequestKennisbank('/filters-kennisbank', parameters, knowledgeBaseResult, true, false, "append");
				}
			})
		}

	}
} export {
	kennisbank
}