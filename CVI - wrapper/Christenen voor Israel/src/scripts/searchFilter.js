import { showMore } from "./showMore";
import gsap from "../../../config/node_modules/gsap";
import {
  Swiper,
  Navigation,
  Pagination,
  Scrollbar,
  Controller,
  Lazy,
  Mousewheel,
} from "../../../config/node_modules/swiper/swiper.esm.js";
Swiper.use([Navigation, Pagination, Scrollbar, Controller, Lazy, Mousewheel]);

async function searchFilter() {
  document.addEventListener("DOMContentLoaded", () => {
    var searchResultsPage = document.querySelector(".search-results");

    if (typeof searchResultsPage != "undefined" && searchResultsPage != null) {
      const debounce = function (func, wait, immediate) {
        var timeout;
        return function () {
          var context = this,
            args = arguments;
          var later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
          };
          var callNow = immediate && !timeout;
          clearTimeout(timeout);
          timeout = setTimeout(later, wait);
          if (callNow) func.apply(context, args);
        };
      };

      var checkboxFilters = searchResultsPage.querySelectorAll(
        ".checkbox-filter input"
      );
      var selectFilters = searchResultsPage.querySelectorAll(
        ".sort-filter select"
      );
      var dateFilterFrom = searchResultsPage.querySelectorAll(
        ".date-filter .date__filter__input.from"
      );
      var dateFilterTo = searchResultsPage.querySelectorAll(
        ".date-filter .date__filter__input.to"
      );

      var resultCount = 0;
      var theEnd;
      var appended;
      var searchInput;
      var i = 0;
      var x = 0;
      var selectedFilters = [];

      function makeRequest(data, type) {
        var container;
        console.log(type);
        if (type === "set_filters") {
          container = searchResultsPage.querySelector("#search__results");
        } else {
          container = searchResultsPage.querySelector(
            "#search__results--items"
          );
        }

        var text = [];
        var i;
        for (i in data) {
          if (data.hasOwnProperty(i)) {
            text.push(i + "=" + encodeURIComponent(data[i]));
          }
        }
        var textState = [];
        var i;
        for (i in data) {
          if (data.hasOwnProperty(i)) {
            if (i != "page") {
              textState.push(i + "=" + encodeURIComponent(data[i]));
            }
          }
        }
        text = text.join("&");
        textState = textState.join("&");

        var url;

        if (type === "set_filters") {
          url = "/search_filters.plate?" + text;
          container = searchResultsPage.querySelector("#search__results");
          searchResultsPage.classList.add("is-loading");
        } else {
          url = "/search?" + text;
          container = searchResultsPage.querySelector(
            "#search__results--items"
          );
          searchResultsPage.classList.add("is-loading");
        }

        var pushStateUrl = "?" + textState;
        window.history.pushState(null, null, pushStateUrl);

        var xhr = window.XMLHttpRequest
          ? new XMLHttpRequest()
          : window.ActiveXObject
          ? new ActiveXObject("Microsoft.XMLHTTP")
          : (alert("Browser does not support XMLHTTP."), false);
        xhr.onreadystatechange = text;
        xhr.open("GET", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(text);
        xhr.onload = function () {
          // var dom = new DOMParser().parseFromString(xhr.response, 'text/html');
          // var searchItems = dom.getElementsByClassName("search-item");
          // resultCount = searchItems.length;
          // setCategoryFilter(searchItems);

          setTimeout(function () {
            // resultCountNumber.innerHTML = resultCount;
            // gsap.to(resultCountWrapper, {
            // 	autoAlpha: 1,
            // 	duration: 1.2,
            // 	ease: "power2.easeIn",
            // });
            container.classList.add("fade-out");
            if (type == "append") {
              container.innerHTML = container.innerHTML + xhr.response;
            } else {
              container.innerHTML = xhr.response;
            }

            if (type === "set_filters") {
              initializeFilters(container);
              showMore(container);
              mobileFilter();

              setTimeout(function () {
                console.log(
                  container.querySelector(".slider-container.fast-search")
                );
                container
                  .querySelectorAll(".slider-container.fast-search")
                  .forEach((element) => {
                    console.log(element);
                    new Swiper(element, {
                      autoplay: false,
                      spaceBetween: 16,
                      slidesPerView: "auto",
                      direction: "horizontal",
                      grabCursor: true,
                      loop: false,
                      breakpoints: {
                        0: {
                          slidesPerView: 1.75,
                        },
                        640: {
                          slidesPerView: 2,
                        },
                        850: {
                          slidesPerView: 3,
                        },
                        1366: {
                          slidesPerView: 4,
                        },
                      },
                      mousewheel: {
                        forceToAxis: true,
                      },
                    });
                  });
              }, 100);
            }

            if (!xhr.response.includes("+==THEEND==+")) {
              setTimeout(function () {
                appended = false;
              }, 1000);
            } else {
              theEnd = true;
            }
            setTimeout(function () {
              searchResultsPage.classList.remove("is-loading");
              container.classList.remove("fade-out");

              // scrollCont.update();
            }, 50);
          }, 50);
        };
        xhr.onerror = function () {
          alert("error");
        };
      }

      function initializeFilters(container) {
        selectedFilters.search = getUrlParameter("search");
        var checkboxFilters = container.querySelectorAll(
          ".checkbox-filter input"
        );
        var dateFilterFrom = container.querySelector(
          ".date__filter__input.from"
        );
        var dateFilterTo = container.querySelector(".date__filter__input.to");
        var selectFilters = container.querySelectorAll(".sort-filter select");
        // var selectedFilters = {};

        var filtersByParentTax = {};

        for (var i = 0; i < checkboxFilters.length; i++) {
          checkboxFilters[i].addEventListener(
            "change",
            debounce(function (e) {
              // Reset some variables when a checkbox is changed
              selectedFilters.search = getUrlParameter("search");
              appended = false;
              theEnd = false;

              var parentTax = this.getAttribute("data-parent");
              var buttonId = this.getAttribute("value");

              if (!filtersByParentTax[parentTax]) {
                filtersByParentTax[parentTax] = [];
              }

              // Toggle the filter value in the array associated with the parentTax
              if (this.checked) {
                filtersByParentTax[parentTax].push(buttonId);
              } else {
                var index = filtersByParentTax[parentTax].indexOf(buttonId);
                if (index !== -1) {
                  filtersByParentTax[parentTax].splice(index, 1);
                }
              }

              // Create a single comma-separated value for each parentTax
              for (var key in filtersByParentTax) {
                selectedFilters[key] = filtersByParentTax[key].join(",");
              }

              makeRequest(selectedFilters, "filter");
            }, 100)
          );
        }

        if (dateFilterFrom !== null) {
          dateFilterFrom.addEventListener(
            "change",
            debounce(function (event) {
              selectedFilters.search = getUrlParameter("search");

              var parentTax = this.getAttribute("data-parent");
              var dateValue = this.value;
              appended = false;
              theEnd = false;

              selectedFilters[parentTax] = dateValue;

              makeRequest(selectedFilters, "filter");
            }, 100)
          );
        }

        if (dateFilterTo !== null) {
          dateFilterTo.addEventListener(
            "change",
            debounce(function (event) {
              selectedFilters.search = getUrlParameter("search");

              var parentTax = this.getAttribute("data-parent");
              var dateValue = this.value;
              appended = false;
              theEnd = false;

              selectedFilters[parentTax] = dateValue;

              makeRequest(selectedFilters, "filter");
            }, 100)
          );
        }

        if (selectFilters !== null) {
          for (var x = 0; x < selectFilters.length; x++) {
            selectFilters[x].addEventListener(
              "change",
              debounce(function (event) {
                selectedFilters.search = getUrlParameter("search");

                appended = false;
                theEnd = false;

                var parentTax = this.getAttribute("data-parent");
                var sortValue = this.options[this.selectedIndex].value;

                selectedFilters[parentTax] = sortValue;

                makeRequest(selectedFilters, "filter");
              }, 100)
            );
          }
        }
      }

      (function () {
        var paramFilters = [];
        // Filter based on parameters in URL
        var param_search = getUrlParameter("search");
        var param_type = getUrlParameter("type");
        var param_cat = getUrlParameter("cat");
        var param_author = getUrlParameter("author");
        var param_date_from = getUrlParameter("from");
        var param_date_to = getUrlParameter("to");
        var param_sort = getUrlParameter("sort");
        var param_filters = getUrlParameter("filters");

        if (param_search) {
          paramFilters["search"] = param_search;

          makeRequest(paramFilters, "set_filters");
        }
      })();

      // Get parameters from URL
      function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
          sURLVariables = sPageURL.split("&"),
          sParameterName,
          i;

        for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split("=");

          if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined
              ? true
              : decodeURIComponent(sParameterName[1]);
          } else {
          }
        }
      }
    }
  });
}

async function mobileFilter() {
  var mobileFilterTrigger = document.querySelector(
    ".search-results__btn:not(.search-results__btn--show)"
  );
  if (
    typeof mobileFilterTrigger === "undefined" ||
    mobileFilterTrigger === null
  )
    return;

  gsap.to(mobileFilterTrigger, {
    autoAlpha: 1,
    duration: 0.4,
    ease: "power2.easeIn",
  });

  var mobileFilter = document.querySelector(
    ".search-results .search-results__filter"
  );
  var closeTriggers = mobileFilter.querySelectorAll(".close-filter");
  var showResultsBtn = mobileFilter.querySelector(".search-results__btn--show");

  mobileFilterTrigger.addEventListener("click", function (e) {
    document.body.classList.add("lock-scroll");
    gsap.to(mobileFilter, {
      autoAlpha: 1,
      duration: 0.4,
      ease: "power2.easeIn",
      onComplete: () => {
        gsap.to(showResultsBtn, {
          pointerEvents: "all",
        });
      },
    });
  });

  closeTriggers.forEach((trigger) => {
    trigger.addEventListener("click", function () {
      gsap.to(mobileFilter, {
        autoAlpha: 0,
        duration: 0.4,
        ease: "power2.easeIn",
        onComplete: () => {
          gsap.to(showResultsBtn, {
            pointerEvents: "none",
          });
        },
      });
      document.body.classList.remove("lock-scroll");
    });
  });
}

export { searchFilter, mobileFilter };
