import {
  Swiper,
  Navigation,
  Pagination,
  Scrollbar,
  Controller,
  Lazy,
  Mousewheel,
} from "../../../config/node_modules/swiper/swiper.esm.js";
Swiper.use([Navigation, Pagination, Scrollbar, Controller, Lazy, Mousewheel]);

async function swiperInstances() {
  var $ = require("../../../config/node_modules/jquery");

  $(document).ready(function () {
    var article_slider = new Swiper(".slider-container.artikelen", {
      autoplay: false,
      spaceBetween: 16,
      direction: "horizontal",
      grabCursor: true,
      loop: false,
      scrollbar: {
        el: ".swiper-scrollbar",
        draggable: false,
      },
      navigation: false,
      breakpoints: {
        0: {
          slidesPerGroup: 1.3,
          slidesPerView: 1.3,
        },
        767: {
          slidesPerView: 2,
        },
        1366: {
          slidesPerView: 3,
          slidesPerGroup: 3,
        },
      },
      mousewheel: {
        forceToAxis: true,
      },
    });

    document
      .querySelectorAll(".swiper-container.content-blocks")
      .forEach((element) => {
        new Swiper(element, {
          autoplay: false,
          spaceBetween: 16,
          direction: "horizontal",
          grabCursor: true,
          loop: false,
          scrollbar: {
            el: element.querySelector(".swiper-scrollbar"),
            draggable: false,
          },
          navigation: false,
          breakpoints: {
            0: {
              slidesPerView: 1.3,
            },
            767: {
              slidesPerView: 2,
            },
            1366: {
              slidesPerView: 3,
            },
          },
          mousewheel: {
            forceToAxis: true,
          },
        });
      });

    var activities_slider = new Swiper(".slider-container.activiteiten", {
      autoplay: {
        delay: 3000,
      },
      slidesPerView: 1,
      slidesPerColumn: 1,
      slidesPerColumnFill: "row",
      spaceBetween: 16,
      direction: "horizontal",
      grabCursor: true,
      loop: false,
      navigation: false,
      breakpoints: {
        0: {
          slidesPerView: 1.2,
        },
        1024: {
          slidesPerView: 2,
        },
        1366: {
          slidesPerView: 4,
        },
      },
      mousewheel: {
        forceToAxis: true,
      },
    });

    var podcast_slider = new Swiper(".slider-container.podcasts", {
      autoplay: false,
      spaceBetween: 16,
      direction: "horizontal",
      grabCursor: true,
      loop: false,
      scrollbar: {
        el: ".swiper-scrollbar",
        draggable: false,
      },
      navigation: false,
      breakpoints: {
        320: {
          slidesPerView: 1,
        },
        768: {
          slidesPerView: 1,
        },
        1024: {
          slidesPerView: 2,
        },
        1366: {
          slidesPerView: 2,
        },
      },
      mousewheel: {
        forceToAxis: true,
      },
    });
    var publication_slider = new Swiper(".slider-container.publications-slider", {
      autoplay: false,
      spaceBetween: 16,
      direction: "horizontal",
      grabCursor: true,
      loop: false,
      scrollbar: {
        el: ".swiper-scrollbar",
        draggable: false,
      },
      navigation: false,
      breakpoints: {
        320: {
          slidesPerView: 1,
        },
        768: {
          slidesPerView: 1,
        },
        1024: {
          slidesPerView: 2,
        },
        1366: {
          slidesPerView: 2,
        },
      },
      mousewheel: {
        forceToAxis: true,
      },
    });

    var video_slider = new Swiper(".slider-container.videos", {
      autoplay: false,
      spaceBetween: 16,
      direction: "horizontal",
      grabCursor: true,
      loop: false,
      scrollbar: {
        el: ".swiper-scrollbar",
        draggable: false,
      },
      navigation: false,
      breakpoints: {
        0: {
          slidesPerGroup: 1.3,
          slidesPerView: 1.3,
        },
        767: {
          slidesPerView: 2,
        },
        1366: {
          slidesPerView: 3,
          slidesPerGroup: 3,
        },
      },
      mousewheel: {
        forceToAxis: true,
      },
    });

    var media_slider = new Swiper(".slider-container.media", {
      autoplay: false,
      spaceBetween: 16,
      slidesPerView: 1,
      direction: "horizontal",
      grabCursor: true,
      loop: true,
      lazy: true,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      mousewheel: {
        forceToAxis: true,
      },
    });

    var fullwidth_slider = new Swiper(".slider-container.fullwidth", {
      autoplay: false,
      spaceBetween: 16,
      slidesPerView: 1.5,
      centeredSlides: true,
      direction: "horizontal",
      grabCursor: true,
      loop: true,
      lazy: true,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      mousewheel: {
        forceToAxis: true,
      },
    });

    var filter_buttons = new Swiper(
      ".slider-container.kennisbank__tax__filter",
      {
        autoplay: false,
        spaceBetween: 16,
        slidesPerView: "auto",
        direction: "horizontal",
        grabCursor: true,
        loop: false,
        breakpoints: {
          0: {
            slidesPerView: 1.75,
          },
          640: {
            slidesPerView: 2,
          },
          850: {
            slidesPerView: 3,
          },
          1366: {
            slidesPerView: 4,
          },
        },
        mousewheel: {
          forceToAxis: true,
        },
      }
    );
  });
}
export { swiperInstances };
