async function postcodeApi () {

    var $ = require("../../../config/node_modules/jquery");

    $(document).ready(function () {
        
        $(".fill-postcode-form .form-field__postcode").on("change", handlePostcodeAPI) //keyup

        function handlePostcodeAPI(event) {
            var formData = event.target.form.dataset;
            var postcode = this.value;
            var stripped_postcode = postcode.replace(/\s+/g, '');
            var signature_key = formData.signature;
            var current_time = formData.time;

            if (validatePostcode(stripped_postcode)) {
                fillInAddress(stripped_postcode, signature_key, current_time);
            }
        }

        function validatePostcode(postcode) {
            var regex = /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i;
            return regex.test(postcode);
        }

        function fillInAddress(postcode, signature, time){
            var result;
            result = $.ajax({
                url: "https://0epdl50es8.execute-api.eu-west-1.amazonaws.com/production/retrievepostcode",
                type: "GET",
                contentType: "application/json",
                data:{
                    postcode: postcode,
                    signature: signature,
                    time: time
                },
                success: function(result){
                    console.log(result);
                    fillForm(result);
                },
                error: function(result){
                    console.log("Error: ", result)
                }
            });
        }

        function fillForm(data){
            $(".form-field__plaats").val(data.city);
            $(".form-field__straat").val(data.street);
        }
    });

} export {
    postcodeApi
}
