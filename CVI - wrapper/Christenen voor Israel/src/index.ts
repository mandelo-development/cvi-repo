import '../../config/node_modules/regenerator-runtime/runtime'
import { filter } from './scripts/filter';
filter();
import {kennisbank} from './scripts/filter_kennisbank'
kennisbank();
import {products} from './scripts/filter_products'
products();
import('./scripts/lazyload').then(({ observeImages }) => {
    observeImages(document);
});
// import {lazyload} from './scripts/lazyload'
// lazyload();
import {cookies} from './scripts/cookies'
cookies();
import {navigation} from './scripts/navigation'
navigation();
import {functions} from './scripts/functions'
functions();
import {swiperInstances} from './scripts/swiper'
swiperInstances();
import {formGeneral} from './scripts/formGeneral'
formGeneral();
import {form} from './scripts/form'
form();
import {navForms} from './scripts/navForm'
navForms();
import {multiform} from './scripts/multistepform'
multiform();
import {postcodeApi} from './scripts/postcode-api'
postcodeApi();
import {fancyboxInstance} from './scripts/fancyboxInstance'
fancyboxInstance();
import {geschiedenis} from './scripts/geschiedenis'
geschiedenis();
import {masonryGrid} from './scripts/grid'
masonryGrid();
import {openstreetmap} from './scripts/openstreetmap'
setTimeout(() => {
    openstreetmap();
}, 1000)
import {search} from './scripts/search'
search();
import {searchFilter} from './scripts/searchFilter';
searchFilter();
import { initPopups } from './scripts/popup';
initPopups();
// import { showMore } from './scripts/showMore';
// showMore();
import {tabs} from './scripts/tabs'
tabs();
import {youtube} from './scripts/youtube'
youtube();
import { scrollIndicator } from './scripts/scrollIndicator';
scrollIndicator();
import './styles/style';
import './styles/elements/contentBlock';

import './scripts/updatecss';

